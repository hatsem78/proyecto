from .settings import API_PREFIX
import json


def global_vars(request):
    return {
        'URL_PREFIX': '',
        'API_PREFIX': API_PREFIX,
    }
