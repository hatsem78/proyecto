from rest_framework import generics, serializers
from django.core.exceptions import ValidationError
#from app_natagua.models import Alumno, Provincia, Localidad
from prestamo.models import Prestamos


class PrestamosPagSerializer(serializers.Serializer):


    class Meta:
        model = Prestamos
        fields = ('id', 'apellido', 'nombre', 'document_number', 'email', 'gender', 'solicitud')

    id = serializers.IntegerField(read_only=True)
    document_number = serializers.CharField(max_length=25)
    apellido = serializers.CharField(required=True, allow_blank=False, max_length=100)
    nombre = serializers.CharField(required=True, allow_blank=False, max_length=100)
    email = serializers.EmailField(max_length=100)
    gender = serializers.CharField(max_length=2)
    solicitud = serializers.CharField(max_length=2)

class PrestamosSerializer(serializers.Serializer):


    class Meta:
        model = Prestamos
        fields = ('id', 'apellido', 'nombre', 'document_number', 'email', 'gender', 'solicitud')

    id = serializers.IntegerField(read_only=True)
    document_number = serializers.CharField(max_length=25)
    apellido = serializers.CharField(required=True, allow_blank=False, max_length=100)
    nombre = serializers.CharField(required=True, allow_blank=False, max_length=100)
    email = serializers.EmailField(max_length=100)
    gender = serializers.CharField(max_length=2)
    solicitud = serializers.CharField(max_length=2)




    def create(self, validated_data):
        """
        Create and return a new `Alumno` instance, given the validated data.
        """
        print(validated_data)
        trans = Prestamos.objects.create(**validated_data)

        return trans

    def update(self, instance, validated_data):
        """
            Update and return an existing `Alumno` instance, given the validated data.
        """

        instance.apellido = validated_data.get('apellido', instance.apellido)
        instance.nombre = validated_data.get('nombre', instance.nombre)
        instance.document_number = validated_data.get('document_number', instance.document_number)
        instance.gender = validated_data.get('gender', instance.gender)
        instance.email = validated_data.get('email', instance.email)
        instance.solicitud = validated_data.get('solicitud', instance.solicitud)


        instance.save()
        return instance

