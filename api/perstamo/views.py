import datetime

from rest_framework.response import Response
from rest_framework.utils import json
from rest_framework.views import APIView
from rest_framework import status, viewsets
from rest_framework import serializers, generics

#from api.alumno.serializers import AlumnoSerializer, AlumnoPagSerializer
#from api.utils import Pagination
#from app_natagua.models import Localidad, Provincia, Alumno
from api.perstamo.serializers import PrestamosPagSerializer, PrestamosSerializer
from api.utils import Pagination
from prestamo.models import Prestamos


class PrestamosList(generics.ListAPIView):
    queryset = snippets = Prestamos.objects.get_queryset().order_by('id')
    serializer_class = PrestamosPagSerializer
    pagination_class = Pagination



class PrestamoAdd(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        snippets = Prestamos.objects.all()
        serializer = PrestamosPagSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):

        serializer = PrestamosSerializer(data=request.data)
        try:
            if serializer.is_valid():
                serializer.save()
                serializer.initial_data['id'] = serializer.instance.id
                return Response(serializer.initial_data, status=status.HTTP_201_CREATED)
        except Exception as error:
            errors = error.args[0]
            return Response({'error': errors}, content_type="application/json")
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PrestamosDetail(APIView):

    def get(self, request, pk, format=None):
        prestamo = Prestamos.objects.get(pk=pk)
        serializer = PrestamosSerializer(prestamo)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        prestamo = Prestamos.objects.get(pk=pk)
        serializer = PrestamosSerializer(prestamo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_object(self, pk):
        try:
            object = Prestamos.objects.get(pk=pk)
            return object
        except Prestamos.DoesNotExist:
            from django.http import Http404
            raise Http404


    def delete(self, request, pk, format=None):
        try:

            snippet = self.get_object(pk)
            snippet.delete()
            return Response(status=status.HTTP_200_OK)
        except Exception as error:
            errors = error.args[0]
            return Response({'error': errors}, content_type="application/json")
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

