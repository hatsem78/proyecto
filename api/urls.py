# coding=utf-8


from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers

from api.perstamo.views import PrestamoAdd, PrestamosList, PrestamosDetail

router = routers.DefaultRouter()

urlpatterns = [

    #url(r'^', include(router.urls)),
    url(r'prestamos_list/', PrestamosList.as_view(), name='prestamos_list'),
    path('prestamo/', PrestamoAdd.as_view()),
    path('prestamos/<int:pk>/', PrestamosDetail.as_view()),
    path('accounts/', include('django.contrib.auth.urls')),  #
    url(r'^api-auth', include('rest_framework.urls', namespace='rest_framework')),
]
