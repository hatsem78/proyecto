from django.db import models

GENDER = (
    ('M', 'MUJER'),
    ('H', 'HOMBRE'),
)

APROBADO = {
    (0, 'AUTORIZADO'),
    (1, 'RECHAZADO')

}


class Prestamos(models.Model):
    """
        Modelo que representa una prestamo
    """
    class Meta:
        verbose_name_plural = "Prestamo"
        ordering = ['nombre']

    id = models.AutoField(primary_key=True, help_text="Id único de Prestamo ")
    nombre = models.CharField('Nombre', max_length=50)
    apellido = models.CharField('Apellido', max_length=50)
    document_number = models.CharField(max_length=25)
    email = models.EmailField(max_length=100, blank=True, null=True, )
    gender = models.CharField(max_length=1, choices=GENDER, null=False, db_index=True)
    solicitud = models.CharField(max_length=1, choices=APROBADO, null=False, db_index=True)


    def __str__(self):
        return '%d: %s %s' % (self.id, self.apellido, self.nombre)