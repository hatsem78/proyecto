from django.shortcuts import render
from django.views import View


def index(request):
    if(request.user.is_authenticated):
        return render(request, 'home.html', )
    return render(request, 'index.html',)

class Home(View):

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated == False:
            return render(request, 'index.html', )
        return render(request, 'home.html',)


