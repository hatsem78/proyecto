Vue.component('vuetable-pagination', window.Vuetable.VuetablePagination);
Vue.component('vuetable', window.Vuetable.Vuetable);

Vue.component('prestamo_action',{
    props:{
        tipo:{
            type: String,
            default: 'prestamo'
        },
        id_update: null,
        titulo_new: {
            type: String,
            default: 'Solicitar Prestamo'
        },
    },
    inject:['$validator'],
    data(){
        return {
            accion: this.tipo,
            titulo: this.titulo_new,
            datos:{
                id:'',
                nombre:'',
                apellido: 'prueba',
                document_number:'132131',
                gender: 'M',
                email: 'prueba@gmai.com',
                solicitud: 0,
                selectSolicitud: null,
            },
            idUpdate: this.id_update
        }
    },
    methods:{
        cancelar: function(){
            this.$emit('close');
        },
        guardar: function () {
            let self = this;
            switch (this.accion) {
                case 'solicitar':
                    self.titulo = 'Solicitar Prestamo';
                    self.controlPrestamo();
                    break;
                case 'prestamo':
                    self.titulo = 'Agregar Prestamo';
                    self.addPrestamo();
                    break;
                case 'prestamo_update':
                    self.titulo = "Modificar Prestamo";
                    self.updatePrestamo();
                    break;
                default:
                    break;
            };
        },
        getPrestamo: function (id) {
            let self = this;
            store.dispatch({type: 'setLoading',value: true});
            HTTP.get(`prestamos/${id}/`)
            .then((response) => {
                self.datos = response.data;
                store.dispatch({type: 'setLoading',value: false});
            })
            .catch((err) => {
                store.dispatch({type: 'setLoading',value: false});
                console.log(err);
            });
        },
        controlPrestamo: function () {
            let self = this;

            store.dispatch({type: 'setLoading',value: true });
            this.$validator.validateAll()
            .then(function(response){
                if (response) {
                    $.ajax({
                        type: 'get',
                        data: self.datos,
                        url: 'http://scoringservice.moni.com.ar:7001/api/v1/scoring/',

                        success: function(data) {

                            if(data.approved){
                                notifier.success('El Prestamo Fue aprobado');
                                self.datos.solicitud = 0;
                            }
                            else{
                                notifier.alert('El Prestamo No fue aprovado');
                                self.datos.solicitud = 1;
                            }
                        },
                        error: function(error) {
                            console.log("FAIL....=================");
                            store.dispatch({type: 'setLoading',value: false });
                        }
                    });
                    self.addPrestamo();
                    store.dispatch({type: 'setLoading',value: false });
                }

            });
        },
        addPrestamo: function () {
            let self = this;

            store.dispatch({type: 'setLoading',value: true });
            this.$validator.validateAll()
            .then(function(response){
                if (response) {

                    HTTP.post('/prestamo/', self.datos)
                    .then((response) => {
                        if(response.data.error){
                            notifier.alert('El Complejo ya se encuentra registrado');
                        }
                        else{
                            if(self.accion !== 'solicitar'){
                                notifier.success('El Complejo se Guardo correctamente');
                                self.accion = 'complejo_update';
                                self.titulo = "Modificar Complejo";
                                self.datos.id = response.data.id;
                            }

                        }

                    })
                    .catch((err) => {
                        store.dispatch({type: 'setLoading',value: false });
                        console.log(err);
                    });
                }
                store.dispatch({type: 'setLoading',value: false });
            });
        },
        updatePrestamo: function () {
            let self = this;

            store.dispatch({type: 'setLoading',value: true });

            HTTP.put(`/prestamos/${self.datos.id}/`, self.datos)
            .then((response) => {
                store.dispatch({type: 'setLoading',value: false });
                if(response.data.error && response.data.error.indexOf('UNIQUE constraint failed: app_natagua_prestamo.nombre') >= 0){
                    notifier.alert('El Prestamo ya se encuentra registrado');
                }
                else{
                    notifier.success('El Prestamo se Guardo correctamente');
                    self.accion = 'Prestamo_update';
                    self.titulo = "Modificar Prestamo";
                    self.datos.id = response.data.id;
                }
            })
            .catch((err) => {
                store.dispatch({type: 'setLoading',value: false });
                console.log(err);
            })
        },

    },
    created: function() {

    },
    watch:{
        titulo: function (val) {
            this.titulo = val;
        }

    },
    mounted: function() {
        let self = this;

        switch (this.accion) {
            case 'prestamo_update':
                self.titulo = "Modificar Prestamo";
                self.getPrestamo(self.idUpdate);
                break;
        };
    },
    template: `
        <div class="card col-md-10">
            <div class="card-header"><h4 class="card-title">  {{ titulo }}</h4></div>
            <div class="card-body">
            
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label>D.N.I*</label>
                            <div  :class="[
                                      { 
                                        'has-error': errors.first('document_number'), 
                                        'has-success': !errors.first('document_number') && datos.document_number !== ''
                                      }, 
                                      ]">
                                      
                                <input 
                                    type="text" 
                                    name="document_number" id="document_number" 
                                    placeholder="D.N.I"  
                                    v-model='datos.document_number'
                                    v-validate="'required:true|max:20|numeric'" 
                                    :class="{
                                        'input': true, 
                                        'has-error': errors.first('document_number') && datos.document_number == '', 
                                        'form-control': true
                                    }"
                                >
                                <div class="errors help-block">
                                    <span v-show="errors.first('document_number')"
                                        class="help error">{{ errors.first('document_number') }}
                                    </span>
                                </div> 
                            </div>
                        </div>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label>Apellido*</label>
                            <div  :class="[
                                      { 
                                        'has-error': errors.first('apellido'), 
                                        'has-success': !errors.first('apellido') && datos.apellido !== ''
                                      }, 
                                      ]">
                                      
                                <input 
                                    type="text" 
                                    name="apellido" id="apellido" 
                                    placeholder="Apellido"  
                                    v-model='datos.apellido'
                                    v-validate="'required:true|max:127|alphanumeric'" 
                                    :class="{
                                        'input': true, 
                                        'has-error': errors.first('apellido') && datos.apellido == '', 
                                        'form-control': true
                                    }"
                                >
                                <div class="errors help-block">
                                    <span v-show="errors.first('apellido')"
                                        class="help error">{{ errors.first('apellido') }}
                                    </span>
                                </div> 
                            </div>  
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label>Nombre*</label>
                            <div  :class="[
                                      { 
                                        'has-error': errors.first('nombre'), 
                                        'has-success': !errors.first('nombre') && datos.nombre !== ''
                                      }, 
                                      ]">
                                      
                                <input 
                                    type="text" 
                                    name="nombre" id="nombre" 
                                    placeholder="Nombre"  
                                    v-model='datos.nombre'
                                    v-validate="'required:true|max:127|alphanumeric'" 
                                    :class="{
                                        'input': true, 
                                        'has-error': errors.first('nombre') && datos.nombre == '', 
                                        'form-control': true
                                    }"
                                >
                                <div class="errors help-block">
                                    <span v-show="errors.first('nombre')"
                                        class="help error">{{ errors.first('nombre') }}
                                    </span>
                                </div> 
                            </div>  
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <div  :class="[
                                      { 
                                        'has-error': errors.first('email'), 
                                        'has-success': !errors.first('email') && datos.email !== ''
                                      }, 
                                      ]">
                                      
                                <input 
                                    type="text" 
                                    name="email" id="email" 
                                    placeholder="Email"  
                                    v-model='datos.email'
                                    v-validate="'required:true|maxCustom:100|email_custom'" 
                                    :class="{
                                        'input': true, 
                                        'has-error': errors.first('email') && datos.email == '', 
                                        'form-control': true
                                    }"
                                >
                                <div class="errors help-block">
                                    <span v-show="errors.first('email')"
                                        class="help error">{{ errors.first('email') }}
                                    </span>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 pl-1">
                    <div class="form-group">
                        <label for="">Sexo*</label>
                        <select v-model="datos.gender" class="form-control">
                            <option value="M">Mujer</option>
                            <option value="H">Hombre</option>
                        </select>
                    </div>
                </div>
                
                
                <div class="col-md-6 pl-1" v-if="accion !== 'solicitar'">
                    <div class="form-group">
                        <label for="">Solicitud</label>
                        <select v-model="datos.solicitud" class="form-control">
                            <option value="0">AUTORIZADO</option>
                            <option value="1">RECHAZADO</option>
                        </select>
                    </div>
                </div>
                
                
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button 
                            v-if="accion !== 'solicitar'"
                            @click="cancelar()"
                            type="button" 
                            class="btn btn-secondary m-progress" 
                        >
                            Cancelar
                        </button>
                        <button 
                            type="submit" 
                            class="btn btn-primary"
                            @click="guardar()"
                        >
                            <spand v-if="accion !== 'solicitar'">Guardar</spand>
                            <spand v-else="">Solicitar</spand>
                        </button>
                    </div>
                </div>
            </div>
        </div>`

});






var prestamo = new Vue({
    el: '#prestamos',
    delimiters: ['${','}'],
    data: {
        titulo:'Agregar Prestamo',
        turno: [],
        tipo: '',
        loading: false,
        currentTurno: {},
        message: null,
        search_term: '',
        fieldes: [
            {
                name: 'id',
                title: 'Id',
                sortField: 'id'
            },
            {
                name: 'apellido',
                title: 'Apellido',
                sortField: 'apellido'
            },
            {
                name: 'nombre',
                title: 'Nombre',
                sortField: 'nombre'
            },
            {
                name: 'document_number',
                title: 'D.N.I',
                sortField: 'document_number'
            },
            {
                name: 'email',
                title: 'Email',
                sortField: 'email'
            },
            {
                name: 'gender',
                title: 'Sexo',
                callback: 'gender',
                sortField: 'gender'
            },
            {
                name: 'solicitud',
                title: 'Solicitud',
                callback: 'solicitud',
                sortField: 'gender'
            },
            {
              name: '__slot:actions',   // <----
              title: 'Actions',
              titleClass: 'center aligned',
              dataClass: 'center aligned'
            }
        ],
        sortOrder: [
            { field: 'name', direction: 'asc' }
        ],
        css: {
            table: {
            tableClass: 'table table-striped table-bordered table-hovered',
            loadingClass: 'loading',
            ascendingIcon: 'glyphicon glyphicon-chevron-up',
            descendingIcon: 'glyphicon glyphicon-chevron-down',
            handleIcon: 'glyphicon glyphicon-menu-hamburger',
        },
            pagination: {
            infoClass: 'pull-left',
            wrapperClass: 'vuetable-pagination pull-right',
            activeClass: 'btn-primary',
            disabledClass: 'disabled',
            pageClass: 'btn btn-border',
            linkClass: 'btn btn-border',
            icons: {
              first: '',
              prev: '',
              next: '',
              last: '',
            },
        }
        },
        action: true,
        showPrestamos: false,
        id_update: 0,
    },
    mounted: function() {
        //this.getTurnos();
    },
    methods: {
        gender: function(value) {
          return value == 'M' ? 'Mujer' : 'Hombre'
        },
        solicitud: function(value) {
          return value == '0' ? 'AUTORIZADO' : 'RECHAZADO'
        },
        onPaginationData: function(paginationData) {
            this.$refs.paginationPrestamos.setPaginationData(paginationData)
        },
        show_prestamo:function(value){
            let self = this;
            if(value){
                self.showPrestamos = value;
            }
            else{
                self.showPrestamos = value;
                self.tipo = 'prestamo';
                self.refresh();
            }
        },
        deletePrestamo: function (id) {
            let self = this;
            store.dispatch({type: 'setLoading',value: true });
            HTTP.delete(`/prestamos/${id}/`)
            .then((response) => {

                store.dispatch({type: 'setLoading',value: false });
                self.refresh();
            })
            .catch((err) => {
                notifier.alert('Error ocurrete: ' + err);
                store.dispatch({type: 'setLoading',value: false });
                console.log(err);
            })
        },
        onChangePage: function(page) {
            this.$refs.vuetable.changePage(page)
        },
        deleteRow: function(rowData){
            this.deletePrestamo(rowData.id);
        },
        addPrestamo: function () {
            let self = this;
            self.titulo = "Agregar Prestamo";
            self.tipo = 'prestamo';
            self.show_prestamo(true);
        },
        editRow: function (value) {
            let self = this;
            self.id_update = value.id;
            self.titulo = "Modificar Prestamo";
            self.tipo = 'prestamo_update';
            self.show_prestamo(true);
        },
        refresh: function() {
            let self = this;
            self.$nextTick(()=>{
              self.$refs.vuetablePrestamos.refresh();
              store.dispatch({type: 'setLoading',value: false });
            })
        },
        onLoading: function() {

        },

        onLoaded:function () {

        }
    }

});